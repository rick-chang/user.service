package service

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		// 获取所有用户
		Name: "GetAllUsers", Method: http.MethodGet, Pattern: "/users", HandlerFunc: GetAllUsers,
	},
	Route{
		// 获取单个用户
		Name: "GetOneUser", Method: http.MethodGet, Pattern: "/users/{id}", HandlerFunc: GetOneUser,
	},
	Route{
		// 新增用户
		Name: "AddUser", Method: http.MethodPost, Pattern: "/users", HandlerFunc: AddUser,
	},
	Route{
		// 删除用户
		Name: "DeleteUser", Method: http.MethodDelete, Pattern: "/users/{id}", HandlerFunc: DeleteUser,
	},
	Route{
		// 更新用户
		Name: "UpdateUser", Method: http.MethodPatch, Pattern: "/users", HandlerFunc: UpdateUser,
	},
}
