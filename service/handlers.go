// Package classification User API.
//
// The purpose of this service is to provide an application
// that is using plain go code to define an API
//
// 		Host: localhost
//     	Version: 0.0.1
//
// swagger:meta
package service

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"user.server/models"

	"crypto/md5"

	"errors"

	"github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

const (
	ErrorResponseCode   = 1000 // 错误响应code
	SuccessResponseCode = 0    // 正确响应code
)

// swagger:parameters getSingleUser
type GetUserParam struct {
	// an id of user info
	//
	// Required: true
	// in: path
	Id int `json:"id"`
}

// User Info
//
// swagger:response UserResponse
type UserWapper struct {
	// in: body
	Body ResponseMessage
}

type ResponseMessage struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func GetOneUser(w http.ResponseWriter, r *http.Request) {
	// swagger:route GET /users/{id} users getSingleUser
	//
	// get a user by userID
	//
	// This will show a user info
	//
	//     Responses:
	//       200: UserResponse
	decoder := json.NewDecoder(r.Body)
	var param GetUserParam
	err := decoder.Decode(&param)
	if err != nil {
		WriteResponse(w, ErrorResponseCode, "request param is invalid, please check!", nil)
		return
	}

	// get user from db
	user, err := models.GetOne(strconv.Itoa(param.Id))
	if err != nil {
		logrus.Warn(err)
		WriteResponse(w, ErrorResponseCode, "failed", nil)
		return
	}
	WriteResponse(w, SuccessResponseCode, "success", user)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	// decode body data into user struct
	decoder := json.NewDecoder(r.Body)
	user := models.User{}
	err := decoder.Decode(&user)
	if err != nil {
		WriteResponse(w, ErrorResponseCode, "user data is invalid, please check!", nil)
		return
	}

	// user data cannot be empty
	if user.Id == 0 || user.Name == "" || user.Password == "" || user.Status > models.UserStatusInValid {
		WriteResponse(w, ErrorResponseCode, "user data is invalid, please check!", nil)
		return
	}

	// check if user exists
	data, err := models.GetUserById(user.Id)
	if err != nil {
		logrus.Warn(err)
		WriteResponse(w, ErrorResponseCode, "query user failed", nil)
		return
	}
	if data.Id == 0 {
		WriteResponse(w, ErrorResponseCode, "user not exists, no need to update", nil)
		return
	}

	// update
	_, err = models.Update(user)
	if err != nil {
		WriteResponse(w, ErrorResponseCode, "update user data failed, please try again!", nil)
		return
	}
	WriteResponse(w, SuccessResponseCode, "update user data success!", nil)
}

// add user data to db
func AddUser(w http.ResponseWriter, r *http.Request) {
	user, err := ValidateFormData(r)
	if err != nil {
		WriteResponse(w, ErrorResponseCode, err.Error(), nil)
		return
	}

	// add
	lastInsertId, err := models.Add(user)
	if err != nil {
		logrus.Warnf("add user failed:%v\n", err)
		WriteResponse(w, ErrorResponseCode, "add user failed, please try again!", nil)
		return
	}

	WriteResponse(w, SuccessResponseCode, fmt.Sprintf("add user success,user id:%d", lastInsertId), nil)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	vals := mux.Vars(r)
	id := vals["id"]

	n, err := strconv.Atoi(id)
	if err != nil {
		WriteResponse(w, ErrorResponseCode, "user id must be an integer", nil)
		return
	} else if n < 0 {
		WriteResponse(w, ErrorResponseCode, "user id must larger than 0", nil)
		return
	}

	affectedRows, err := models.Delete(id)
	if err != nil {
		logrus.Warnf("delete user failed, error:%v\n", err)
		WriteResponse(w, ErrorResponseCode, "delete user failed,please try again!!!", nil)
		return
	}

	if affectedRows == 0 {
		WriteResponse(w, SuccessResponseCode, "no user need to delete", nil)
		return
	}
	WriteResponse(w, SuccessResponseCode, "delete user success", nil)
}

func WriteResponse(w http.ResponseWriter, code int, message string, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	resp := ResponseMessage{Code: code, Message: message, Data: data}
	b, err := json.Marshal(resp)
	if err != nil {
		logrus.Warnf("error when marshal response message, error:%v\n", err)
	}
	w.Write(b)
}

func ValidateFormData(r *http.Request) (user models.User, err error) {
	user = models.User{}
	// 获取表单数据
	username := r.PostFormValue("name")
	if username == "" {
		return user, errors.New("add user failed, empty field:name")
	}

	data, err := models.GetUserByName(username)
	if err != nil {
		return user, err
	}
	if data.Id != 0 {
		return user, errors.New("add user failed, user already exists")
	}

	// set username
	user.Name = username

	// set password
	password := r.PostFormValue("password")
	if password == "" {
		return user, errors.New("add user failed, empty field:password")
	}
	user.Password = fmt.Sprintf("%x", md5.Sum([]byte(password)))

	// set status
	status, err := strconv.Atoi(r.PostFormValue("status"))
	if err != nil || status > models.UserStatusInValid {
		return user, errors.New(fmt.Sprintf("cannot parse field: status: %d, error:%v\n", status, err))
	}
	user.Status = status
	return user, nil
}

func GetAllUsers(w http.ResponseWriter, r *http.Request) {
	users, err := models.GetAll()
	if err != nil {
		logrus.Warn(err)
		WriteResponse(w, ErrorResponseCode, "failed", nil)
		return
	}

	WriteResponse(w, SuccessResponseCode, "success", users)
}
