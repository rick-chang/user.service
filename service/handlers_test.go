package service

import (
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
	"user.server/models"

	"bytes"

	"time"

	. "github.com/smartystreets/goconvey/convey"
	"github.com/tidwall/gjson"
)

func TestGetAllUsers(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/user/all", nil)

	rw := httptest.NewRecorder()
	NewRouter().ServeHTTP(rw, req)
	b, err := ioutil.ReadAll(rw.Body)
	if err != nil {
		t.Fatal("read response body failed")
	}

	code := gjson.Get(string(b), "code").Int()
	if code != 0 {
		t.Fail()
	}
}

func TestGetOneUser(t *testing.T) {
	// 测试正常响应
	Convey("Given a http request for /user/1", t, func() {
		req := httptest.NewRequest(http.MethodGet, "/user/1", nil)
		resp := httptest.NewRecorder()
		Convey("When request is handled by router", func() {
			NewRouter().ServeHTTP(resp, req)
			Convey("The result code should be 0", func() {
				b, _ := ioutil.ReadAll(resp.Body)
				code := gjson.Get(string(b), "code").Int()
				So(code, ShouldEqual, SuccessResponseCode)
			})
		})
	})

	// 测试错误响应
	Convey("Given a http request for /user/0", t, func() {
		req := httptest.NewRequest(http.MethodGet, "/user/0", nil)
		resp := httptest.NewRecorder()
		Convey("When request is handled by router", func() {
			NewRouter().ServeHTTP(resp, req)
			Convey("The result code should be 1000", func() {
				b, _ := ioutil.ReadAll(resp.Body)
				code := gjson.Get(string(b), "code").Int()
				So(code, ShouldEqual, ErrorResponseCode)
			})
		})
	})
}

func TestAddUser(t *testing.T) {
	// username 为空的情况
	Convey("Given a http request for /user/add with empty username", t, func() {
		form := make(url.Values)
		form.Add("name", "")
		req := httptest.NewRequest(http.MethodPost, "/user/add", strings.NewReader(form.Encode()))
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		resp := httptest.NewRecorder()
		Convey("When the request handled by router", func() {
			NewRouter().ServeHTTP(resp, req)
			Convey("The result code should be 1000", func() {
				b, _ := ioutil.ReadAll(resp.Body)
				code := gjson.Get(string(b), "code").Int()
				So(code, ShouldEqual, ErrorResponseCode)
			})
		})
	})

	// password 为空的情况
	Convey("Given a http request for /user/add with empty password", t, func() {
		form := make(url.Values)
		form.Add("name", "rand_name"+randomString(3))
		form.Add("password", "")
		req := httptest.NewRequest(http.MethodPost, "/user/add", strings.NewReader(form.Encode()))
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		resp := httptest.NewRecorder()
		Convey("When the request handled by router", func() {
			NewRouter().ServeHTTP(resp, req)
			Convey("The result code should be 1000", func() {
				b, _ := ioutil.ReadAll(resp.Body)
				code := gjson.Get(string(b), "code").Int()
				So(code, ShouldEqual, ErrorResponseCode)
			})
		})
	})

	//  status错误的情况
	Convey("Given a http request for /user/add with invalid status", t, func() {
		form := make(url.Values)
		form.Add("name", "rand_name"+randomString(3))
		form.Add("password", "111111")
		form.Add("status", "2")
		req := httptest.NewRequest(http.MethodPost, "/user/add", strings.NewReader(form.Encode()))
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		resp := httptest.NewRecorder()
		Convey("When the request handled by router", func() {
			NewRouter().ServeHTTP(resp, req)
			Convey("The result code should be 1000", func() {
				b, _ := ioutil.ReadAll(resp.Body)
				code := gjson.Get(string(b), "code").Int()
				So(code, ShouldEqual, ErrorResponseCode)
			})
		})
	})

	// 正常创建
	Convey("Given a http request for /user/add", t, func() {
		form := make(url.Values)
		form.Add("name", "rand_name_"+time.Now().Format("20060102150405"))
		form.Add("password", "tttttt")
		form.Add("status", "1")

		req := httptest.NewRequest(http.MethodPost, "/user/add", strings.NewReader(form.Encode()))
		req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
		resp := httptest.NewRecorder()
		Convey("When the request is handled by router", func() {
			NewRouter().ServeHTTP(resp, req)
			Convey("The result code should be 0", func() {
				b, _ := ioutil.ReadAll(resp.Body)
				code := gjson.Get(string(b), "code").Int()
				So(code, ShouldEqual, SuccessResponseCode)
			})
		})
	})

}

func TestDeleteUser(t *testing.T) {
	Convey("Given a http request for /user/delete/1000", t, func() {
		req := httptest.NewRequest(http.MethodDelete, "/user/delete/1000", nil)
		resp := httptest.NewRecorder()
		Convey("When request handled by router", func() {
			NewRouter().ServeHTTP(resp, req)
			Convey("The result code should be 0", func() {
				b, _ := ioutil.ReadAll(resp.Body)
				code := gjson.Get(string(b), "code").Int()
				So(code, ShouldEqual, SuccessResponseCode)
			})
		})
	})
}

func randomString(n int) string {
	letterBytes := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func TestUpdateUser(t *testing.T) {
	// test empty username
	Convey("Given a http for /user/update", t, func() {
		user := models.User{Id: 1, Name: "", Password: "nnnnn", Status: 1}
		data, err := json.Marshal(user)
		if err != nil {
			t.Fatal(err)
		}

		req, _ := http.NewRequest(http.MethodPost, "/user/update", bytes.NewReader(data))
		resp := httptest.NewRecorder()
		Convey("When request handled by router", func() {
			NewRouter().ServeHTTP(resp, req)
			Convey("result code should be 1000", func() {
				b, _ := ioutil.ReadAll(resp.Body)
				code := gjson.Get(string(b), "code").Int()
				So(code, ShouldEqual, ErrorResponseCode)
			})
		})
	})

	// normal test
	Convey("Given a http request for /user/update", t, func() {
		user := models.User{Id: 1, Name: "tomN...", Password: "nnnnn", Status: 1}
		data, err := json.Marshal(user)
		if err != nil {
			t.Fatal(err)
		}

		req, _ := http.NewRequest(http.MethodPost, "/user/update", bytes.NewReader(data))
		resp := httptest.NewRecorder()
		Convey("When request handled by router", func() {
			NewRouter().ServeHTTP(resp, req)
			Convey("result code should be 0", func() {
				b, _ := ioutil.ReadAll(resp.Body)
				code := gjson.Get(string(b), "code").Int()
				So(code, ShouldEqual, SuccessResponseCode)
			})
		})
	})
}
