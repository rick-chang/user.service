package service

import "github.com/gorilla/mux"

func NewRouter() *mux.Router {
	r := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		r.Methods(route.Method).Name(route.Name).Path(route.Pattern).HandlerFunc(route.HandlerFunc)
	}
	return r
}
