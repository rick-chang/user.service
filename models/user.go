package models

import (
	"user.server/db"
)

const (
	UserStatusValid   = 0 // 有效账户
	UserStatusInValid = 1 // 无效账户
)

type User struct{
	// Required: true
	Id       int    `json:"id"`
	Name     string `json:"name"`
	Password string `json:"password"`
	Status   int    `json:"status"`
}


// get all users
func GetAll() ([]User, error) {
	var users []User

	conn := db.Conn
	rows, err := conn.Query("select id,name,password,status from user")
	if err != nil {
		return []User{}, err
	}
	defer rows.Close()
	
	for rows.Next() {
		user := User{}
		err := rows.Scan(&user.Id, &user.Name, &user.Password, &user.Status)
		if err != nil {
			return []User{}, err
		}
		users = append(users, user)
	}

	return users, nil
}

// get single user by id
func GetOne(id string) (User, error) {
	user := User{}
	err := db.Conn.QueryRow("select id,name,password,status from user where id = ?", id).Scan(&user.Id, &user.Name, &user.Password, &user.Status)
	if err != nil {
		return User{}, err
	}
	return user, nil
}

func GetUserByName(username string) (User, error) {
	user := User{}
	err := db.Conn.QueryRow("select id from user where name = ?", username).Scan(&user.Id)
	if err != nil {
		return User{}, err
	}
	return user, nil
}

func GetUserById(id int) (User, error) {
	user := User{}
	err := db.Conn.QueryRow("select id from user where id = ?", id).Scan(&user.Id)
	if err != nil {
		return User{}, err
	}
	return user, nil
}

func Add(user User) (lastInsertId int64, err error) {
	result, err := db.Conn.Exec("insert into `user` (`name`,`password`, `status`) values (?, ?, ?)", user.Name, user.Password, user.Status)
	if err != nil {
		return 0, err
	}

	lastInsertId, err = result.LastInsertId()
	if err != nil {
		return 0, err
	}
	return lastInsertId, nil
}

func Delete(id string) (affectedRows int64, err error) {
	result, err := db.Conn.Exec("update `user` set status = ? where id = ?", UserStatusInValid, id)
	if err != nil {
		return 0, err
	}

	affectedRows, err = result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return affectedRows, nil
}

func Update(user User) (affectedRows int64, err error) {
	result, err := db.Conn.Exec("update `user` set name = ?, password = ?,status = ? where id = ?", user.Name, user.Password, user.Status, user.Id)
	if err != nil {
		return 0, err
	}

	affectedRows, err = result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return affectedRows, nil
}
