package models

import (
	"reflect"
	"testing"
	"time"
)

func TestGetOne(t *testing.T) {
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		args    args
		want    User
		wantErr bool
	}{
		{"GetOneUser", args{id: "2"}, User{Id: 2, Name: "jerry", Password: "222222", Status: 1}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetOne(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetOne() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetOne() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUserByName(t *testing.T) {
	type args struct {
		username string
	}
	tests := []struct {
		name    string
		args    args
		want    User
		wantErr bool
	}{
		{"GetUserByName", args{username: "jerry"}, User{Id: 2}, false},
		{"GetUserWithWrongName", args{username: "wrongName"}, User{Id: 0}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetUserByName(tt.args.username)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetUserByName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetUserByName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetUserById(t *testing.T) {
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		args    args
		want    User
		wantErr bool
	}{
		{"withIdExists", args{id: 1}, User{Id: 1}, false},
		{"withIdNotExists", args{id: 0}, User{Id: 0}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetUserById(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetUserById() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetUserById() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAdd(t *testing.T) {
	type args struct {
		user User
	}
	tests := []struct {
		name             string
		args             args
		wantLastInsertId int64
		wantErr          bool
	}{
		{"withCorrectData", args{user: User{
			Name:     "rand_name_" + time.Now().Format("20060102150405"),
			Password: time.Now().Format("20060102150405"),
			Status:   1,
		}},
			10,
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := Add(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}

func TestUpdate(t *testing.T) {
	type args struct {
		user User
	}
	tests := []struct {
		name             string
		args             args
		wantAffectedRows int64
		wantErr          bool
	}{
		{"update", args{user: User{1, "update_test", "test", 1}}, 1, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotAffectedRows, err := Update(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotAffectedRows != tt.wantAffectedRows {
				t.Errorf("Update() = %v, want %v", gotAffectedRows, tt.wantAffectedRows)
			}
		})
	}
}
