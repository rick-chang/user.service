package db

import (
	_ "github.com/go-sql-driver/mysql"
	"database/sql"
	"fmt"
)

var Conn *sql.DB
var dsn = "root:111111@/mydb"

func init() {
	var err error
	Conn, err = sql.Open("mysql", dsn)
	if err != nil {
		panic(fmt.Sprintf("connect to database error:%v\n", err))
	}
}

func Close(){
	Conn.Close()
}
