package main

import (
	"log"
	"net/http"
	"user.server/service"
	"user.server/db"
)

func main() {
	r := service.NewRouter()
	http.Handle("/", r)

	defer db.Close()
	log.Fatal(http.ListenAndServe(":8001", nil))
}
